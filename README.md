# MirrorHall 
A low-latency virtual display app for GNOME

![Logo](data/icons/hicolor/scalable/apps/eu.nokun.MirrorHall.svg)

## How does it work?

MirrorHall creates virtual second monitors over the network using UDP video 
streaming among Linux desktop or mobile devices. It is optimized for low latency, 
and is based on `gstreamer` and GNOME's `mutter` screen sharing APIs.


## Disclaimer

This is an *early proof of concept* and has several drawbacks. It is *nowhere* 
near usable at production level, and the video stream is *not* encrypted over
the network. Proceed at your own risk.

## Supported devices

This app supports hardware encoding for some ARM and x86 devices, more specifically:

- Modern Intel and AMD CPUs (vaapi)
- Qualcomm CPUs (via v4l2)

If you have an unsupported CPU/GPU configuration, a software fallback will be used
at the expense of some performance and CPU load.


## Screenshots

![Stream Mode](data/screenshots/stream.png)
![Mirror Mode](data/screenshots/mirror.png)

## License

Mirror Hall is released under GNU GPL v3. See `COPYING` for more information.

## Credits

The current Mirror Hall icon was designed by Tobias Bernard.