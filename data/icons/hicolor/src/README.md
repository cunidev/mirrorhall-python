I included the source of my logo design for the sake of transparency. 
This blueprint can be opened with the GNOME Icon Preview app, and was done in Inkscape.

Any modifications will NOT be automatically compiled to the target icon - please export
from Icon Preview directly to there.
