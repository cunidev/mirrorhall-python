import gi
from gi.repository import Adw, Gtk, Gio

gi.require_version("Adw", "1")

@Gtk.Template(resource_path='/eu/nokun/MirrorHall/mh_prefs_window.ui')
class MhPreferencesWindow(Adw.PreferencesWindow):
    __gtype_name__ = 'MhPreferencesWindow'

    pref_enable = Gtk.Template.Child()
    prefs_group_transmission = Gtk.Template.Child()
    prefs_group_playback = Gtk.Template.Child()
    prefs_group_playback_empty = Gtk.Template.Child()
    
    # Text entries
    pref_force_encode_pipeline = Gtk.Template.Child()
    pref_force_decode_pipeline = Gtk.Template.Child()

    # Number entries
    pref_sink_port = Gtk.Template.Child()
    pref_framerate = Gtk.Template.Child()

    # Switches
    pref_force_software_encode = Gtk.Template.Child()
    pref_force_software_decode = Gtk.Template.Child()
    pref_x264_zerolatency = Gtk.Template.Child()
    pref_bitrate = Gtk.Template.Child()
    pref_use_clappersink = Gtk.Template.Child()
    pref_capture_input = Gtk.Template.Child()

    # Combo rows
    pref_x264_preset = Gtk.Template.Child()
    pref_codec = Gtk.Template.Child()
    pref_container_format = Gtk.Template.Child()
    pref_desktop_type = Gtk.Template.Child()


    pref_entries = []
    pref_switches = []
    pref_spins = []
    pref_combos = []

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.gsettings = Gio.Settings.new("eu.nokun.MirrorHall")
        self.connect('realize', self.on_realize)

    def on_realize(self, widget):
        self.pref_entries = [
            ('force-encode-pipeline', self.pref_force_encode_pipeline),
            ('force-decode-pipeline', self.pref_force_decode_pipeline),
        ]
        self.pref_switches = [
            ('enable-advanced-preferences', self.pref_enable),
            ('force-encode-software', self.pref_force_software_encode),
            ('force-decode-software', self.pref_force_software_decode),
            ('x264-zerolatency', self.pref_x264_zerolatency),
            ('use-clappersink', self.pref_use_clappersink),
            ('capture-input', self.pref_capture_input),
        ]

        self.pref_spins = [
            ('sink-port', self.pref_sink_port),
            ('framerate', self.pref_framerate),
            ('bitrate', self.pref_bitrate),
        ]

        self.pref_combos = [
            ('x264-preset', self.pref_x264_preset),
            ('codec', self.pref_codec),
            ('container-format', self.pref_container_format),
            ('desktop-type', self.pref_desktop_type),
        ]

        for key, widget in self.pref_entries:
            self.gsettings.bind(key, widget, 'text', Gio.SettingsBindFlags.DEFAULT)
        for key, widget in self.pref_switches:
            self.gsettings.bind(key, widget, 'active', Gio.SettingsBindFlags.DEFAULT)
        for key, widget in self.pref_spins:
            self.gsettings.bind(key, widget, 'value', Gio.SettingsBindFlags.DEFAULT)

        self.pref_enable.connect('notify::active', self.toggle_scary_prefs)
        self.toggle_scary_prefs(None, None)


    def toggle_scary_prefs(self, w, p):
        danger_zone = self.gsettings.get_boolean('enable-advanced-preferences')

        self.prefs_group_transmission.set_visible(danger_zone)
        self.prefs_group_playback.set_visible(danger_zone)
        self.prefs_group_playback_empty.set_visible(not danger_zone)

        if not danger_zone:
            print('Resetting advanced settings to default')
            for key, widget in self.pref_entries + self.pref_switches + self.pref_spins + self.pref_combos:
                self.gsettings.reset(key)