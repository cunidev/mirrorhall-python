'''
A bunch of error dialogs for different components in the app
'''

import gi, sys
from gi.repository import Gio, Adw, Gtk

class MhErrorDialogs:
    mutter_warning = """
<span size="large" weight="800">⚠️ Disclaimer</span>

Mirror Hall uses a GNOME API which is still experimental, and has been found to crash some versions of GNOME including the latest 47.x.

This is <a href="https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/4148">an upstream bug</a>, and we do not recommend using Mirror Hall in production contexts until a fix is found.

This bug appeared later than GNOME 46, when this app was initially released. Apologies for the inconvenience.
"""

    gsettings = Gio.Settings.new("eu.nokun.MirrorHall")

    def __init__(self, parent_window):
        self.parent_window = parent_window

    # Construct a stream error
    def stream_error(self, e):
        def _on_response(dialog, response_id):
            if response_id == 'close':
                dialog.destroy()
            elif response_id == 'reset':
                for key in self.gsettings.list_keys():
                    print("Resetting key %s" % key)
                    self.gsettings.reset(key)
                dialog.destroy()

        dialog = Adw.MessageDialog.new(
            self.parent_window,
            "⚠️ Cannot Start Stream", 
            "There's been an issue: \n\n<tt>%s</tt>\n\nMake sure your system is using newer GStreamer and GNOME versions (41+)."%e
        )
        dialog.set_body_use_markup(True)
        dialog.add_response("close", "Close")
        dialog.add_response("reset", "_Reset Settings")
        dialog.connect("response", _on_response)
        dialog.show()


    # Occurs for in-pipeline errors (e.g. missing decoders)
    def pipeline_error(self, e):
        def _on_response(dialog, response_id):
            if response_id == 'close':
                dialog.destroy()
            elif response_id == 'reset':
                for key in self.gsettings.list_keys():
                    print("Resetting key %s" % key)
                    self.gsettings.reset(key)
                dialog.destroy()

        dialog = Adw.MessageDialog.new(
            self.parent_window,
            "Your pipeline blew up. 💣", "There's been an issue: \n\n<tt>%s</tt>\n\n Please check the console for a more meaningful log."%e
        )
        dialog.set_body_use_markup(True)
        dialog.add_response("close", "Close")
        dialog.add_response("reset", "_Reset Settings")
        dialog.connect("response", _on_response)
        dialog.show()


    # Warn Mutter users about Mirror Hall crashing GNOME 47.x
    def mutter_47_warning(self):
        box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        box.set_vexpand(True)
        box.add_css_class("p-20")
        label = Gtk.Label()
        label.set_markup(self.mutter_warning)
        label.set_wrap(True)
        box.append(label)
        button = Gtk.Button.new()
        button.set_label('I understand the risks')
        button.connect('clicked', lambda w: dialog.close())
        button.add_css_class('destructive-action')
        box.append(button)

        dialog = Adw.Dialog()
        dialog.set_child(box)
        dialog.present(self.parent_window)
        dialog.show()

    # Generic fatal error dialog - currently stateful (exits the app)
    def backend_error(self, e):
        dialog = Adw.MessageDialog.new(
            None,
            "Could not start backend", str(e)
        )
        dialog.set_body_use_markup(True)
        dialog.connect("response", lambda dialog, response_id: sys.exit(1))
        dialog.add_response("close", "Close")
        app.add_window(dialog)
        dialog.present()
        dialog.show()