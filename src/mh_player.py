'''
GStreamer video player widget
'''
# TODO: decouple widget (GstPipelineViewer(pipeline)) from pipeline and negotiation logic!

from gi.repository import Gst, Gtk, Adw, Gio
import gi

gi.require_version('Gst', '1.0')
from .libpipeline import PipelineService
from .mh_error_dialogs import MhErrorDialogs

class GstMirrorWidget(Gtk.Box):
    def __init__(self, port_sink, parent_window, capture_input=False):
        super(GstMirrorWidget, self).__init__()
        self.connect('realize', self._on_realize)
        self.connect('destroy', self._on_destroy)
        self.port_sink = port_sink
        self.parent_window = parent_window
        self.capture_input = capture_input
        self.gsettings = Gio.Settings.new("eu.nokun.MirrorHall")
        self.set_tooltip_text("Click to toggle fullscreen")

    def _on_realize(self, _):
        def _on_message(bus, message):
            print("GstMirrorWidget: message received: %s (type %s)" %
                  (message, message.type))
            if message.type == Gst.MessageType.EOS:
                self._bin.set_state(Gst.State.NULL)
                self._bin = None
            elif message.type == Gst.MessageType.ERROR:
                self._bin.set_state(Gst.State.NULL)
                err, debug = message.parse_error()
                print('Error: %s' % err, debug)
                self._bin = None
            return True

        try:
            self._bin = PipelineService().create_sink_pipeline(self.port_sink)
            # self._bin = Gst.parse_bin_from_description('rtspsrc location="rtsp://localhost:8554/live" latency=0 buffer-mode=auto ! parsebin ! queue ! decodebin ', True)
            pipeline = Gst.Pipeline()
            pipeline.get_bus().connect('message', _on_message)
            gtksink = pipeline.get_factory().make(PipelineService().find_sink(), "sink")
            pipeline.add(self._bin)
            pipeline.add(gtksink)
            # Link the pipeline to the sink that will display the video.
            self._bin.link(gtksink)

            try:
                widget = gtksink.props.widget
                gtksink.props.widget.show()
            except:
                paintable = gtksink.props.paintable
                widget = Gtk.Picture.new_for_paintable(paintable)
            widget.set_size_request(300, 300)
            widget.set_hexpand(True)
            widget.set_vexpand(True)
            
            widget.add_css_class("mirror-widget")

            # connect to input event handlers via event controller
            if self.capture_input:
                gesture = [
                    ( Gtk.GestureClick, ['pressed', 'released'], self.handle_input_event ),
                    ( Gtk.GestureSwipe, ['swipe'], self.handle_input_event ),
                    ( Gtk.GestureDrag, ['drag-begin', 'drag-update', 'drag-end'], self.handle_input_event ),
                    ( Gtk.GestureLongPress, ['pressed'], self.handle_input_event ),
                    ( Gtk.GesturePan, ['pan'], self.handle_input_event ),
                    ( Gtk.GestureRotate, ['angle-changed'], self.handle_input_event ),
                    ( Gtk.GestureStylus, ['down', 'up', 'motion'], self.handle_input_event ),
                    ( Gtk.GestureZoom, ['scale-changed'], self.handle_input_event ),
                ]

                for g in gesture:
                    gesture = g[0]()
                    for e in g[1]:
                        gesture.connect(e, g[2])
                    widget.add_controller(gesture)


            self.prepend(widget)
            widget.show()
            # Start the video
            pipeline.set_state(Gst.State.PLAYING)
        
        except Exception as e:
            print("GstMirrorWidget: error creating pipeline: %s" % e)
            MhErrorDialogs(self.parent_window).pipeline_error(e)

    def handle_input_event(self, gesture, a, b, c = None, d = None):
        print("GstMirrorWidget: input event: %s %s %s %s %s." % (gesture, a, b, c, d))
        return True

    # Kills the open pipeline when the player widget is destroyed.
    def _on_destroy(self, widget):
        print("GstMirrorWidget: destroying widget")
        self._bin.set_state(Gst.State.NULL)
        self._bin = None


