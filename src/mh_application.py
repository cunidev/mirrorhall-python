'''
Entry point of the Mirror Hall application
'''
import sys, subprocess, socket, gi, random, os

gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')

from gi.repository import Gtk, Gdk, Gio, Adw, GLib
from .mh_window import MirrorhallWindow
from .mh_prefs_window import MhPreferencesWindow
from .mh_error_dialogs import MhErrorDialogs
from .mh_teleport_dialog import MhTeleportDialog
from .libcast import MirrorNetworkService, MhNetworkDeviceData
from .libmirror import MirrorService

# Main application singleton
class MirrorhallApplication(Adw.Application):
    def __init__(self, net_service: MirrorNetworkService, dbus_service: MirrorService, sink_port: int, debug):
        session_id = '-sess'+str(random.random() * 100000) if debug else ''# hack to allow launching many instances at the same time
        if debug:
            print("Running in MH_MULTI debug mode.. Multiple app instances allowed.")
        
        super().__init__(application_id='eu.nokun.MirrorHall' + str(session_id),
                         flags=Gio.ApplicationFlags.DEFAULT_FLAGS)
        
        self.rtsp_server_endpoint = None
        self.net_service = net_service
        self.dbus_service = dbus_service
        self.sink_port = sink_port
        self.create_action('quit', lambda *_: self.quit(), ['<primary>q'])
        self.create_action('about', self.on_about_action)
        self.create_action('preferences', self.on_preferences_action)
        self.create_action('connect-manually', self.on_connect_manually_action, ['<primary>c'])
        self.rtsp_action = self.create_action('connect-rtsp', self.on_connect_rtsp_action, ['<primary>r'])
        self.create_action('set-max-res', self.on_set_max_res_action)
        self.create_action('switch-role', lambda *_: self.props.active_window.switch_role_cb(None), ['<primary>x'])
        if self.gnome_settings_available():
            self.create_action('open-gnome-settings', self.on_open_gnome_display_settings_action, ['<primary>d'])


    def do_activate(self):
        win = self.props.active_window
        
        if not win:
            win = MirrorhallWindow(net_service=self.net_service, dbus_service=self.dbus_service, sink_port=self.sink_port, application=self)
        win.present()
        self._update_resolutions(win.get_surface())

        settings = Gio.Settings.new("eu.nokun.MirrorHall")
        if settings.get_boolean('first-run') == True:
            MhErrorDialogs(win).mutter_47_warning()
            # settings.set_boolean('first-run', False)

        self.inhibit(
            None,
            Gtk.ApplicationInhibitFlags.SUSPEND | Gtk.ApplicationInhibitFlags.LOGOUT,
            'A virtual display might be active.'
        )

    def on_about_action(self, widget, _):
        about = Adw.AboutWindow(transient_for=self.props.active_window,
                                application_name='Mirror Hall',
                                application_icon='eu.nokun.MirrorHall',
                                developer_name='Raffaele T. (nokun)',
                                version='0.1.1',
                                developers=['Raffaele T. (nokun)'],
                                website='https://nokun.eu',
                                copyright='© 2024 nokun.eu. Released under GNU GPL v3')
        about.present()

    def on_preferences_action(self, widget, _):
        window = MhPreferencesWindow(transient_for=self.props.active_window)
        window.show()


    def on_set_max_res_action(self, widget, _):
        print('app.set_max_res action activated')

    def on_connect_manually_action(self, widget, _):
        def on_manual_connect(w):
            dialog.host_entry.remove_css_class("error")
            dialog.port_entry.remove_css_class("error")

            try:
                port = int(dialog.port_entry.get_text())
            except:
                dialog.port_entry.add_css_class("error")
                return

            c = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            c.settimeout(1)
            try:
                host = dialog.host_entry.get_text()
                # TODO: why does this not fail e.g. if host = ""?
                ip = socket.gethostbyname(host)
                c.connect((ip, port))
                c.close()
                # TODO: check if it's a mirrorhall device, negotiate, etc.
            except:
                print("Can't connect to host")
                dialog.host_entry.add_css_class("error")
                return

            virtual_width = 1280
            virtual_height = 720
            target_id = '%s:%d'%(host,port)

            device = MhNetworkDeviceData(target_id, host, port, [host], ['added manually'], 'screen-shared-symbolic', '??')
            try:
                self.dbus_service.start_stream(device, virtual_width, virtual_height, False)
            except Exception as e:
                MhErrorDialogs(self.props.active_window).stream_error(e)
                return

            self.net_service.device_list[target_id] = device
            self.props.active_window._glib_update_device_list()

            print('Streaming on port %d'%port)
            dialog.close()

        dialog = MhTeleportDialog(self.props.active_window, on_manual_connect)
        dialog.show()


    def on_connect_rtsp_action(self, widget, _):
        dialog = Gtk.Dialog(title="Cast to RTSP", transient_for=self.props.active_window)
        dialog.set_modal(True)
        dialog.set_default_response(Gtk.ResponseType.OK)
        dialog.set_resizable(False)
        header_bar = Gtk.HeaderBar()
        header_bar.set_show_title_buttons(False)


        content_area = dialog.get_content_area()

        if not self.rtsp_server_endpoint:
            banner_text = "☠️ RTSP latency is awful. This should be a last resort."
        else:
            banner_text = "📡 You already have one active stream at <a href=\"rtsp://localhost:8554/%s\">rtsp://localhost:8554/%s</a>"%(
                self.rtsp_server_endpoint, self.rtsp_server_endpoint
            )
        banner = Adw.Banner()
        banner.set_use_markup(True)
        banner.set_title(banner_text)
        banner.set_revealed(True)
        content_area.append(banner)
        
        container_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        container_box.set_margin_top(10)
        container_box.set_margin_bottom(10)
        container_box.set_margin_start(20)
        container_box.set_margin_end(20)
        container_box.append(Gtk.Label.new("Create a virtual screen over RTSP, e.g. for use with VLC"))

        width_entry = Gtk.Entry()
        width_entry.set_text('1280')
        width_entry.set_placeholder_text("width")
        width_entry.set_hexpand(True)
        width_entry.set_max_width_chars(4)
        height_entry = Gtk.Entry()
        height_entry.set_text('720')
        height_entry.set_placeholder_text("height")
        height_entry.set_hexpand(True)
        height_entry.set_max_width_chars(4)

        endpoint_label = Gtk.Label()
        endpoint_label.set_use_markup(True)
        def set_endpoint_label(_=None):
            endpoint_label.set_markup(
                "A server will start on <a href=\"rtsp://localhost:8554/%s\">rtsp://localhost:8554/%s</a>."%(
                    endpoint_entry.get_text(),
                    endpoint_entry.get_text(),
                )
            )

        endpoint_entry = Gtk.Entry()
        endpoint_entry.set_text('mirror')
        endpoint_entry.set_placeholder_text("endpoint")
        endpoint_entry.set_hexpand(False)
        endpoint_entry.set_max_width_chars(8)
        endpoint_entry.connect("changed", set_endpoint_label)

        entry_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        entry_box.add_css_class("linked")
        entry_box.append(width_entry)
        entry_box.append(height_entry)
        entry_box.append(endpoint_entry)
        container_box.append(entry_box)
        content_area.append(container_box)

        container_box.append(endpoint_label)
        set_endpoint_label()


        def handle_manual_connect(button):
            width_entry.remove_css_class("error")
            height_entry.remove_css_class("error")
            endpoint_entry.remove_css_class("error")

            try:
                width = int(width_entry.get_text())
            except:
                width_entry.add_css_class("error")
                return
            try:
                height = int(height_entry.get_text())
            except:
                height_entry.add_css_class("error")
                return
            
            endpoint = endpoint_entry.get_text()

            if endpoint == "" or not endpoint.isalnum():
                endpoint_entry.add_css_class("error")
                return
            
            if self.dbus_service.get_session('rtsp:%s'%endpoint, 8554)[0]:
                endpoint_entry.add_css_class("error")
                return

            try:
                self.dbus_service.start_stream(None, width, height, False, endpoint)
            except Exception as e:
                MhErrorDialogs(self.props.active_window).stream_error(e)
                return
                
            # self.net_service.device_list[target_id] = device
            # self.props.active_window._glib_update_device_list()

            print('Enabled RTSP server on rtsp://localhost:8554/%s'%endpoint)
            self.rtsp_server_endpoint = endpoint
            self.props.active_window.set_rtsp_server_endpoint(endpoint)
            self.rtsp_action.set_enabled(False)
            dialog.close()

        connect_button = Gtk.Button.new_with_label("Stream")
        connect_button.add_css_class("suggested-action")
        cancel_button = Gtk.Button.new_with_label("Cancel")
        connect_button.connect("clicked", handle_manual_connect)
        cancel_button.connect("clicked", lambda button: dialog.close())
        header_bar.pack_start(cancel_button)
        header_bar.pack_end(connect_button)

        dialog.set_titlebar(header_bar)
        dialog.show()

    def gnome_settings_available(self):
        try:
            gio_app = Gio.AppInfo.create_from_commandline("gnome-control-center", None, Gio.AppInfoCreateFlags.NONE)
            return True
        except:
            try:
                # see if binary for g-c-c is available in host
                subprocess.check_output(["flatpak-spawn", "--host", "gnome-control-center", "--version"])
                return True
            except:
                return False
    def on_open_gnome_display_settings_action(self, widget, _):
        print("Opening GNOME Control Center, Display Settings")
        try:
            gio_app = Gio.AppInfo.create_from_commandline("gnome-control-center", None, Gio.AppInfoCreateFlags.NONE)
            gio_app.launch([], None)
        except:
            # flatpak-spawn --host gnome-control-center display
            try:
                subprocess.Popen(["flatpak-spawn", "--host", "gnome-control-center", "display"])
            except:
                print("gnome-control-center not found")


    # Adds an application action
    def create_action(self, name, callback, shortcuts=None):
        action = Gio.SimpleAction.new(name, None)
        action.connect("activate", callback)
        self.add_action(action)
        if shortcuts:
            self.set_accels_for_action(f"app.{name}", shortcuts)
        return action

    # Update supported resolutions
    # TODO: is this correct? embed better compatibility
    def _update_resolutions(self, surface):
        display = Gdk.Display.get_default()
        monitor = display.get_monitor_at_surface(surface)
        scale_factor = monitor.get_scale_factor()
        geometry = monitor.get_geometry()
        x = geometry.width * scale_factor
        y = geometry.height * scale_factor

        self.net_service.set_resolutions([
            "{}x{}".format(x,y),
            "{}x{}".format(int(x*2/3),int(y*2/3)), # hardcoded lower-res scaled stream
            "{}x{}".format(int(x/2),int(y/2)) # hardcoded half-res stream
        ])

# Application entry point
def main(version):
    settings = Gio.Settings.new("eu.nokun.MirrorHall")
    # sink_port for now is randomly chosen between 6901 and 6999. yes, you can be unlucky.
    if settings.get_int('sink-port') == 0:
        sink_port = 6901 + random.randint(0, 98)

    try:
        net_service = MirrorNetworkService(sink_port)
        dbus_service = MirrorService()
        net_service.mdns_start_discover()
        print("mirrorhall: network backend loaded.")
    except Exception as e:
        def on_activate(app):
            dialog = MhErrorDialogs(None).backend_error(e)

        app = Adw.Application.new('eu.nokun.MirrorHall', Gio.ApplicationFlags.FLAGS_NONE)
        app.connect('activate', on_activate)
        app.run()
        return 1


    debug = os.getenv("MH_MULTI")
    app = MirrorhallApplication(net_service, dbus_service, sink_port, debug)
    return app.run(sys.argv)