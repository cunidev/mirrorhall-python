'''
Represents a network device item in the device selector grid
'''

from gi.repository import Adw
from gi.repository import Gtk


@Gtk.Template(resource_path='/eu/nokun/MirrorHall/mh_device_item.ui')
class MhDeviceItem(Gtk.ToggleButton):
    __gtype_name__ = 'MhDeviceItem'

    device_name = Gtk.Template.Child()
    device_info_1 = Gtk.Template.Child()
    device_info_2 = Gtk.Template.Child()
    device_icon = Gtk.Template.Child()
    spinner = Gtk.Template.Child()

    def __init__(self, device=None, **kwargs):
        # add properties : device_name, device_role, device_info_1
        super().__init__(**kwargs)
        self.device = device
        if device:
            self.set_device_name(device.name)
            self.set_device_icon(device.icon)
            self.set_device_info_1("{}".format(device.resolutions[0]))
        #self.set_device_info_2("{}:{}".format(device.hostname, device.port))

        self.set_active(False)
        self.set_spinning(False)

    def set_spinning(self, spinning: bool):
        self.spinner.set_visible(spinning)
        #self.checkbox.set_visible(not spinning)

    def set_device_name(self, name: str):
        self.device_name.set_text(name)
        
    def set_device_info_1(self, info: str):
        self.device_info_1.set_text(info)

    def set_device_info_2(self, info: str):
        self.device_info_2.set_text(info)

    def set_device_icon(self, icon: str):
        self.device_icon.set_from_icon_name(icon)

