# Mirror Hall CLI frontend

import argparse, sys, gi
gi.require_version('Gst', '1.0')
gi.require_version('Gio', '2.0')
from gi.repository import Gst, GLib, Gio

from .libcast import MhNetworkDeviceData, MirrorNetworkService
from .libmirror import MirrorService
from .libpipeline import PipelineService

# Entry point of CLI frontend
def run(version):
    print('Mirrorhall CLI - version %s' % version)
    print('Tip: run with GST_DEBUG=3 for debugging output\n')
    parser = argparse.ArgumentParser(description='Mirrorhall CLI')
    parser.add_argument('command', type=str, help='Command to run')
    parser.add_argument('args', type=str, nargs='*', help='Arguments to pass to command')
    args = parser.parse_args()
    if args.command == 'cast':
        if len(args.args) < 2:
            print('The command `mirrorhall cast` requires at least two arguments: [host] [port].')
            print('Please run `mirrorhall help` for more information.')
            return
        cast(args.args[0], int(args.args[1]), int(args.args[2]) if len(args.args) > 2 else 1280, int(args.args[3]) if len(args.args) > 3 else 720)
    elif args.command == 'make-mirror':
        make_mirror(int(args.args[0]) if len(args.args) > 0 else 800, int(args.args[1]) if len(args.args) > 1 else 480)
    elif args.command == 'sink':
        if len(args.args) < 2:
            print('The command `mirrorhall sink` requires at least one arguments: [port].')
            print('Please run `mirrorhall help` for more information.')
            return
        sink(int(args.args[0]), len(args.args) > 1 and args.args[1] == 'print')
    else:
        print('Usage: mirrorhall [command] [args]\n')

        print('mirrorhall cast [ip] [port] [width?] [height?]')
        print(' -> creates virtual screen and streams it to ip:port\n')

        print('mirrorhall make-mirror [width] [height]')
        print(' -> creates virtual screen and outputs PipeWire handle\n')

        print('mirrorhall sink [port] [print?]')
        print(' -> starts accepting virtual streams on [port]. If `print` is given,')
        print('    returns the GStreamer pipeline needed to build a MirrorHall sink.')

# Casts to the specified host and port 
def cast(host: str, port: int, width=1280, height=720):
    print("Casting to %s:%d" % (host, port))
    print("Resolution: %dx%d" % (width, height))
    print("Press Ctrl+C to stop\n")

    target_id = '%s:%d'%(host,port)
    device = MhNetworkDeviceData(target_id, host, port, host, [], '', '')
    dbus_service = MirrorService()
    dbus_service.start_stream(device, width, height)
    loop = GLib.MainLoop()
    loop.run()

# Creates a virtual Mutter display for the given resolution
def make_mirror(width = 1280, height = 720):
    print("Creating virtual screen with resolution %dx%d" % (width, height))
    print("Press Ctrl+C to destroy\n")

    dbus_service = MirrorService()
    dbus_service.start_stream(None, None, None, True)
    loop = GLib.MainLoop()
    loop.run()

# Starts a GStreamer video sink for the given port in a new window
def sink(port: int, dry=False):
    print("Sink started on port %d - press Ctrl+C to stop" % port)

    net_service = MirrorNetworkService(port)
    net_service.mdns_start_announce()
    pipeline_desc = PipelineService().create_sink_pipeline_description(port) + ' ! autovideosink'
    print("Tip: You can use this command to replicate the pipeline outside of Mirror Hall.\n")
    print("$ gst-launch-1.0 %s" % pipeline_desc)
    if not dry:
        pipeline = Gst.parse_bin_from_description(pipeline_desc, True)
        pipeline.set_state(Gst.State.PLAYING)
        loop = GLib.MainLoop()
        loop.run()