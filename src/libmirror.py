'''
Interfaces with Mutter via D-Bus to create and capture virtual displays
'''

import gi
gi.require_version('Gst', '1.0')
gi.require_version('Adw', '1')
from gi.repository import Gst, GLib, Gio, Adw

from .libpipeline import PipelineService

class MirrorService:
    gsettings = Gio.Settings.new("eu.nokun.MirrorHall")
    pipeline_service = PipelineService()

    # Several outgoing streams can exist at the same time. 
    # We identify each as { 'host:port' : { pipeline, session } }
    sessions = {}
    
    def __init__(self) -> None:
        Gst.init(None)
        GLib.threads_init()

    # Starts playing a pipeline
    def start_pipeline(self, host, port, node_id, format_element, bitrate):
        def on_message(bus, message):
            type = message.type
            if type == Gst.MessageType.EOS or type == Gst.MessageType.ERROR:
                #self.stop_stream(host, port)
                print("libmirror: stream on %s:%s has been terminated."%(host, port))
                self.stop_stream(host, port)

        pipeline = self.pipeline_service.create_sender_pipeline(node_id, format_element, host, port)
        pipeline.get_bus().connect('message', on_message)
        pipeline.set_state(Gst.State.PLAYING)

        #if self.get_session(host, port)[0]:
        #    self.stop_stream(host, port)
        self.push_session(host, port)
        self.sessions["%s:%d"%(host, port)]['pipeline'] = pipeline

    # Starts a stream on the given device
    # dry: perform a dry run, do not start the stream
    # rtsp_endpoint: if set, start an RTSP server on the given port instead of a raw stream
    def start_stream(self, device, virtual_width, virtual_height, dry = False, rtsp_endpoint=None):
        def _on_pipewire_stream_added(connection, sender_name, object_path, interface_name, signal_name, parameters, user_data):
            # sender id -> sender_object.
            bitrate = self.gsettings.get_int("bitrate") if self.gsettings.get_int("bitrate") else 6500
            framerate = self.gsettings.get_int("framerate") if self.gsettings.get_int("framerate") else 30
            node_id = parameters[0]
            if not dry:
                try:
                    format_element = "video/x-raw,max-framerate=%d/1,width=%d,height=%d !"%(framerate, virtual_width, virtual_height)
                    if rtsp_endpoint:
                        self.pipeline_service.start_rtsp_server(node_id, rtsp_endpoint)
                    else:
                        self.start_pipeline(host, port, node_id, format_element, bitrate)
                except Exception as e:
                    self.stream_error(e)
                    return
            else:
                print("PipeWire stream added with node ID %s" % node_id)

        if not device and dry:
            host = "dummy"
            port = 0
        elif rtsp_endpoint:
            host = "rtsp:%s"%rtsp_endpoint
            port = 8554
        elif not device and not dry:
            raise Exception('No valid device has been provided')
        else:
            host = device.ips[0] # TODO: handle multiple IPs / interfaces
            port = device.port
        
        if self.get_session(host, port)[0]:
            self.stop_stream(host, port)

        framerate = self.gsettings.get_int("framerate")
        if not framerate:
            framerate = 30


        screen_cast = Gio.DBusProxy.new_for_bus_sync(
            Gio.BusType.SESSION,
            Gio.DBusProxyFlags.NONE,
            None,
            "org.gnome.Mutter.ScreenCast",
            "/org/gnome/Mutter/ScreenCast",
            "org.gnome.Mutter.ScreenCast",
            None)

        session_path = screen_cast.call_sync(
            "CreateSession",
            GLib.Variant("(a{sv})",
            ({},)),
            Gio.DBusCallFlags.NONE,
            -1,
            None)
        print("Screencast session path: %s"%(session_path.get_data_as_bytes().get_data().decode("utf-8")))
        (session, stream_path) = self.start_session(host, port, session_path)

        stream = Gio.DBusProxy.new_for_bus_sync(
            Gio.BusType.SESSION,
            Gio.DBusProxyFlags.NONE,
            None,
            "org.gnome.Mutter.ScreenCast",
            stream_path.get_data_as_bytes().get_data().decode("utf-8"),
            "org.gnome.Mutter.ScreenCast.Stream",
            None)


        dbus_connection = Gio.bus_get_sync(Gio.BusType.SESSION, None)
        dbus_connection.signal_subscribe(
            None,
            "org.gnome.Mutter.ScreenCast.Stream",
            "PipeWireStreamAdded",
            None,
            None,
            Gio.DBusSignalFlags.NONE,
            _on_pipewire_stream_added,
            None)

        session.call_sync("Start", None, Gio.DBusCallFlags.NONE, -1, None)

    # Terminates the stream
    def stop_stream(self, host, port):
        (session, pipeline) = self.get_session(host, port)
        if pipeline is not None:
            print("libmirror: draining pipeline on %s:%s"%(host, port))
            pipeline.send_event(Gst.Event.new_eos())
            pipeline.set_state(Gst.State.NULL)

        if session:
            session.call_sync("Stop", None, Gio.DBusCallFlags.NONE, -1, None)
            print("stream: stopped session %s"%session)
        #self.sessions["%s:%d"%(host, port)]['session'] = session
        self.delete_session(host, port)

    # Starts a screen casting session on Mutter
    def start_session(self, host, port, session_path):
        if self.get_session(host, port):
            self.stop_stream(host, port)

        self.push_session(host, port)

        session = Gio.DBusProxy.new_for_bus_sync(
            Gio.BusType.SESSION,
            Gio.DBusProxyFlags.NONE,
            None,
            "org.gnome.Mutter.ScreenCast",
            session_path.get_data_as_bytes().get_data().decode("utf-8"),
            "org.gnome.Mutter.ScreenCast.Session",
            None)

        stream_path = session.call_sync("RecordVirtual",
            GLib.Variant("(a{sv})", ({"is-platform": GLib.Variant("b", True), "cursor-mode": GLib.Variant("u", 1)},)),
            Gio.DBusCallFlags.NONE, -1, None)
        print("Screencast stream path: %s"%stream_path.get_data_as_bytes().get_data().decode("utf-8"))

        self.sessions["%s:%d"%(host, port)]['session'] = session

        return (session, stream_path)

    # Terminates all open streams
    def stop_all_streams(self):
        for addr in list(self.sessions.keys()):
            try:
                self.stop_stream(addr.split(':')[0], int(addr.split(':')[1]))
            except:
                pass


    # Adds a new session to the list
    # TODO: replacement logic?
    def push_session(self, host, port):
        if "%s:%d"%(host, port) not in self.sessions:
            self.sessions["%s:%d"%(host, port)] = {}

    # Fetches an existing session identifier, or a (None,None) tuple if it is not found 
    def get_session(self, host, port):
        d = self.sessions["%s:%d"%(host, port)] if "%s:%d"%(host, port) in self.sessions else {}
        return ( d['session'] if 'session' in d else None, d['pipeline'] if 'pipeline' in d else None)
    
    # Removes a session from the list - TODO: replacement/deletion logic?
    def delete_session(self, host, port):
        if "%s:%d"%(host, port) in self.sessions:
            del self.sessions["%s:%d"%(host, port)]

    # Checks the state of an open stream
    def is_streaming(self, device):
        # TODO: handle multiple IPs
        return self.get_session(device.ips[0], device.port)[1] is not None