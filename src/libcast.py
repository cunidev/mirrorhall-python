'''
Provides support layer for mDNS and device discoverability for on-network video streaming
'''

from zeroconf import ServiceBrowser, Zeroconf, ServiceInfo, ServiceStateChange
import random, string, socket, netifaces

# Exposes the device on the network, via mDNS
class MirrorNetworkService:
    def __init__(self, sink_port: int) -> None:
        self.resolutions = ["1280x720"]
        self.server = None
        self.device_list = {}
        random_nonce = "".join(random.choices(string.ascii_uppercase + string.ascii_lowercase + string.digits, k=4))
        self.instance_name = self.get_hostname() + "-" + random_nonce
        self.data = MhNetworkDeviceData(
            name = self.get_hostname(),
            hostname = self.instance_name,
            port = sink_port,
            ips = [],
            resolutions = self.resolutions,
            icon = "computer",
            supported_codecs = ["mirrorhall-h264-udp"],
        )

        self.zeroconf = Zeroconf()

    # Announce this device via mDNS
    def mdns_start_announce(self) -> None:
        info = ServiceInfo(
            "_mhcast._tcp.local.",
            self.instance_name + "._mhcast._tcp.local.",
            addresses=self.get_iface_ips(),
            port=self.data.port,
            properties={
                "mhcast_name": self.data.name,
                "mhcast_hostname": self.data.hostname,
                "mhcast_resolutions": ",".join(self.data.resolutions),
                "mhcast_supported_codecs": ",".join(self.data.supported_codecs),
                "mhcast_version": "0.0.1-alpha",
            },
        )
        self.zeroconf.register_service(info)

    # Stop announcing mDNS
    def mdns_stop_announce(self) -> None:
        self.zeroconf.unregister_all_services()

    # Start discovery on mDNS
    def mdns_start_discover(self) -> None:
        self.browser = ServiceBrowser(self.zeroconf, "_mhcast._tcp.local.", handlers=[self._on_service_state_change])
    
    # Stop discovery on mDNS
    def mdns_stop_discover(self) -> None:
        self.browser.cancel()
        self.device_list = {}

    def set_resolutions(self, resolutions) -> None:
        self.data.resolutions = resolutions
        print("Supported resolutions updated to ", resolutions)
        # TODO: update service props dynamically if we're announcing it

    # Handler for added and removed mDNS services
    def _on_service_state_change(self, zeroconf, service_type, name, state_change):
        if state_change is ServiceStateChange.Added:
            info = zeroconf.get_service_info(service_type, name)
            print("New service: " + name)
            if info:
                props = info.properties
                if props:
                    if b"mhcast_hostname" in props.keys():
                        dname = props[b"mhcast_name"].decode("utf-8")
                        hostname = info.server
                        ips = info.parsed_addresses()
                        port = info.port
                        resolutions = props[b"mhcast_resolutions"].decode("utf-8").split(",")
                        is_phone = resolutions[0].split("x")[0] < resolutions[0].split("x")[1]
                        icon = "phone" if is_phone else "computer"
                        supported_codecs = props[b"mhcast_supported_codecs"].decode("utf-8").split(",")
                        self.device_list["%s:%s"%(hostname,port)] = MhNetworkDeviceData(dname, hostname, port, ips, resolutions, icon, supported_codecs)
                        
                        
                        print("Resolved a new service: " + dname)
                        print("  host: " + hostname + ":" + str(port) + " (" + str(ips) + ")")
                        print("  addresses: " + str(info.parsed_addresses()))
                        print("  properties: " + str(props))
        elif state_change is ServiceStateChange.Removed:
            print("Removed a service: " + name)
            try:
                # TODO: attempt to disconnect before removing?
                del self.device_list[name]
            except:
                pass
        else:
            print("Unknown state change: " + str(state_change))

    # Return the device name, or empty string if not set
    def get_hostname(self) -> str:
        return socket.gethostname()

    # Return the IP address this device
    def get_iface_ips(self) -> list:
        ips = []
        for iface in netifaces.interfaces():
            if iface == "lo":
                continue
            iface_data = netifaces.ifaddresses(iface)
            if netifaces.AF_INET in iface_data:
                for addr in iface_data[netifaces.AF_INET]:
                    if addr['addr'] != "127.0.0.1":
                        ips.append(addr['addr'])
        if len(ips) > 0:
            return ips
        raise Exception("No network interface found. Are you connected to a network?")

# Represents a Mirror Hall device on the network
class MhNetworkDeviceData:
    def __init__(self, name, hostname, port, ips, resolutions, icon, supported_codecs) -> None:
        self.name = name
        self.hostname = hostname
        self.port = port
        self.ips = ips
        self.resolutions = resolutions
        self.icon = icon
        self.supported_codecs = supported_codecs